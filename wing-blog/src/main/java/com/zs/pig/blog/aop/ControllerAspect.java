package com.zs.pig.blog.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.zs.pig.blog.serviceImpl.BlogServiceImpl;

@Aspect
@Component
public class ControllerAspect {
	static Logger LOG = LogManager.getLogger(ControllerAspect.class.getName());
	
	@Pointcut(value = "execution(public * com.test.controller.*.*(..))")
	public void recordLog() {
	}
	
	/**
	 * 所拦截方法执行之前执行
	 */
	@Before("recordLog()")
	public void before(){
		System.out.println("before---------------------------------------");
	}	
	
	/**
	 * 同时在所拦截方法的前后执行
	 *	在ProceedingJoinPoint.proceed()前后加逻辑
	 */
	@Around("execution(public * com.zs.pig.blog.serviceImpl.*.*(..))")
	public void around(ProceedingJoinPoint joinPoint) throws Throwable {		
		long startTime = System.currentTimeMillis();		
		joinPoint.proceed();	//个人理解是去执行相应的Controller方法	
		long costTime = System.currentTimeMillis() - startTime;		
		
		String methodName = joinPoint.getSignature().getName();
		//记录执行请求耗时
		LOG.info(methodName + " finished ! Cost : " + costTime + " ms.");		
	}	
	
	/**
	 * 所拦截方法执行之后执行
	 */
	@After("recordLog()")
	public void after(){
		System.out.println("after---------------------------------------");
	}
}