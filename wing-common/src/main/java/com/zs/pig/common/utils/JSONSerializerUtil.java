//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.zs.pig.common.utils;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.DeserializationConfig.Feature;

public class JSONSerializerUtil {
    public JSONSerializerUtil() {
    }

    public static ObjectMapper getObjectMaper() {
        return JSONSerializerUtil.MapperInstance.mapper;
    }

    public static String toJsonString(Object obj) {
        try {
            return getObjectMaper().writeValueAsString(obj);
        } catch (IOException var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public static <T> T toObject(String jsonString, Class<T> cls) {
        try {
            return getObjectMaper().readValue(jsonString, cls);
        } catch (IOException var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static <T> T toObject(byte[] bytes, Class<T> cls) {
        try {
            return getObjectMaper().readValue(bytes, cls);
        } catch (IOException var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static <T> T unserialize(byte[] content, Class<T> cls) throws Exception {
        return toObject(content, cls);
    }

    public static Map<String, Object> unserializeToMap(byte[] content) throws Exception {
        if(content == null) {
            return null;
        } else {
            Map map = (Map)toObject(content, Map.class);
            return map;
        }
    }

    public static Map<String, Object> unserialize(String content) throws Exception {
        return StringUtils.isEmpty(content)?null:(Map)toObject(content, HashMap.class);
    }

    public static Map<String, byte[]> unserialize(byte[] content) throws Exception {
        return (Map)toObject(content, HashMap.class);
    }

    public static ArrayList unserialize2(byte[] bytes) throws Exception {
        return bytes.length < 1?null:(ArrayList)toObject(bytes, ArrayList.class);
    }

    public static String serialize(Map<String, Object> map) throws Exception {
        return map == null?null:new String(getObjectMaper().writeValueAsBytes(map));
    }

    public static String serialize(Object object) throws Exception {
        return object == null?null:new String(getObjectMaper().writeValueAsBytes(object));
    }

    public static byte[] serializeToBytes(Object obj) throws Exception {
        return obj == null?null:getObjectMaper().writeValueAsBytes(obj);
    }

    public static byte[] serializemap(Map objmap) throws Exception {
        return objmap == null?null:getObjectMaper().writeValueAsBytes(objmap);
    }

    public static String serialize2(List<String> list) throws Exception {
        return list == null?null:new String(getObjectMaper().writeValueAsBytes(list));
    }

    public static void main(String[] args) throws Exception {
//        RedisLink redisLink = new RedisLink("192.168.1.231", 6379, "redis");
//        boolean size = true;
//        User user = new User();
//        user.setUid(UUID.randomUUID().toString());
//        user.setUsername(UUID.randomUUID().toString());
//        user.setHeadurl(UUID.randomUUID().toString());
//        user.setCreatetime(UUID.randomUUID().toString());
//        user.setFiletoken(UUID.randomUUID().toString());
//        user.setIs_recommend(UUID.randomUUID().toString());
//        user.setLastlogintime(UUID.randomUUID().toString());
//        user.setMatchphone(UUID.randomUUID().toString());
//        System.out.println("-start serialize2 list------------------------------");
//        String rStr = redisLink.getJedis().get("effectlist:ios:iPhone6,2:10.0.2:0.12.140.320");
//        Map m = PHPSerializerOldUtil.unserializeToMap(rStr.getBytes());
//        System.out.println(m);
//        String str2 = serialize(m);
//        System.out.println(str2);
    }

    private static class MapperInstance {
        public static ObjectMapper mapper = new ObjectMapper();

        private MapperInstance() {
        }

        static {
            Feature var10001 = Feature.FAIL_ON_UNKNOWN_PROPERTIES;
            mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
    }
}
