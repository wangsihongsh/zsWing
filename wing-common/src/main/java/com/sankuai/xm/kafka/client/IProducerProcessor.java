/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client;

import com.sankuai.xm.kafka.client.utils.FutureCallback;

public abstract interface IProducerProcessor<K, V>
{
  public abstract void sendAsyncMessage(V paramV, K paramK, FutureCallback paramFutureCallback)
    throws Exception;

  public abstract void sendAsyncMessage(V paramV, FutureCallback paramFutureCallback)
    throws Exception;

  public abstract void close()
    throws Exception;
}