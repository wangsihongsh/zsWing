/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class KafkaBuildFactory {
	protected static final String PRODUCER_CONFIG = "producer.properties";
	protected static final String CONSUMER_CONFIG = "consumer.properties";

	protected static Properties init0(String propertiesName) throws IOException {
		ClassLoader cl = KafkaBuildFactory.class.getClassLoader();
		InputStream inputStream;
		if (cl != null)
			inputStream = cl.getResourceAsStream(propertiesName);
		else {
			inputStream = ClassLoader.getSystemResourceAsStream(propertiesName);
		}
		Properties dbProps = new Properties();
		try {
			dbProps.load(inputStream);
		} finally {
			inputStream.close();
		}
		return dbProps;
	}
}