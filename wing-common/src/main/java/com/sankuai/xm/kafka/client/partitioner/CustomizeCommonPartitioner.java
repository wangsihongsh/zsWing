//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.sankuai.xm.kafka.client.partitioner;

import kafka.producer.Partitioner;
import kafka.utils.VerifiableProperties;

public class CustomizeCommonPartitioner implements Partitioner {
    public CustomizeCommonPartitioner(VerifiableProperties props) {
    }

    public int partition(Object key, int numPartitions) {
        return (key.hashCode() & 2147483647) % numPartitions;
    }
}
