/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.exception;

public class ConsumerRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 5122623760553747260L;

	public ConsumerRuntimeException() {
	}

	public ConsumerRuntimeException(String message) {
		super(message);
	}

	public ConsumerRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConsumerRuntimeException(Throwable cause) {
		super(cause);
	}
}