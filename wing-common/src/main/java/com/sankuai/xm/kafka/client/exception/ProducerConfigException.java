/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.exception;

public class ProducerConfigException extends RuntimeException {
	private static final long serialVersionUID = 5922623760553747260L;

	public ProducerConfigException() {
	}

	public ProducerConfigException(String message) {
		super(message);
	}

	public ProducerConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProducerConfigException(Throwable cause) {
		super(cause);
	}
}