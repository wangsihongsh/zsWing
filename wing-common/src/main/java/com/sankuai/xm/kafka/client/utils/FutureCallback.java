/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client.utils;

public abstract interface FutureCallback<V>
{
  public abstract void onSuccess(V paramV);

  public abstract void onFailure(V paramV, Throwable paramThrowable);
}