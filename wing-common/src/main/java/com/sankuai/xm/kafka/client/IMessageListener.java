/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.sankuai.xm.kafka.client;

import java.io.Serializable;

public abstract interface IMessageListener<T extends Serializable>
{
  public abstract void recvMessage(byte[] paramArrayOfByte);
}