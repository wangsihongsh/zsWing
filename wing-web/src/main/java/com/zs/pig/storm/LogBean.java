package com.zs.pig.storm;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import net.oschina.common.search.Searchable;

public class LogBean implements Searchable{
	 private long id;
	private String id1;
	private String sn;
	private String order;
	private String type;
	private String datetime;
	private String address;
	private String user;
	private String event;
	private String params;
	public LogBean(){}
	public LogBean(long id, String id1, String sn, String order, String type
			) {
		super();
		this.id = id;
		this.id1 = id1;
		this.sn = sn;
		this.order = order;
		this.type = type;
		
	}
	public LogBean(long id, String id1, String sn, String order, String type,
			String datetime, String address, String user, String event,
			String params) {
		super();
		this.id = id;
		this.id1 = id1;
		this.sn = sn;
		this.order = order;
		this.type = type;
		this.datetime = datetime;
		this.address = address;
		this.user = user;
		this.event = event;
		this.params = params;
	}

	@Override
    public long id() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
	public String getId1() {
		return id1;
	}
	public void setId1(String id1) {
		this.id1 = id1;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	@Override
	public int compareTo(Searchable o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	 public long getId() {
		return id;
	}
	@Override
	    public List<String> storeFields() {
	        return Arrays.asList("id","id1","sn","order","type","datetime","address","user","event","params");
	    }

	    @Override
	    public List<String> indexFields() {
	    	return Arrays.asList("id1","sn","order","type","datetime","address","user","event","params");
	    }
	@Override
	public float boost() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Map<String, String> extendStoreDatas() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, String> extendIndexDatas() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<? extends Searchable> ListAfter(long id, int count) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String toString() {
		return "LogBean [id=" + id + ", id1=" + id1 + ", sn=" + sn + ", order="
				+ order + ", type=" + type + ", datetime=" + datetime
				+ ", address=" + address + ", user=" + user + ", event="
				+ event + ", params=" + params + "]";
	}
	
	
	
}
