/** Powered By 北京甜园科技, Since 2016 - 2020 */
package com.zs.pig.web.monitor.controller;


import java.io.IOException;

import net.oschina.common.search.IndexHolder;

import org.apache.lucene.search.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.zs.pig.storm.LogBean;
/**
 * 
 * @author 沈专 2016-12-1 14:27:20
 * @Email: [email]
 * @version [version]
 *	授权产品管理
 */
@Controller
@RequestMapping("luence")
public class LuenceController {

	
	@RequestMapping
	public String toLicense(Model model){
		return "other/luence/luence";
	}
	
	
	
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public String list(int pageNum,int pageSize,String id, Model model) throws IOException {
		  String indexFolder = "C:/sz/git2/zsWing/wing-web/target/classes/index";
		  
	      
		  IndexHolder  holder = IndexHolder.init(indexFolder);
		  Query query =null;
//		  if("".endsWith(sn) || sn ==null){
//			  
//		  }else{
//			  query = SearchHelper.makeQuery("sn", sn, 10.f);
//		  }
		    
		   Page<LogBean> list = holder.findLogBean(LogBean.class, query, null, null, pageNum, pageSize);
		   Page<LogBean> sublist = holder.findLogBean(LogBean.class, query, null, null, pageNum, pageSize);
		 //  int total=holder.count(Arrays.asList(LogBean.class), null, null);
		
		   int total=list.size();
				   //holder.count(Arrays.asList(LogBean.class,Post.class), null, null);
		if(total>0){
			sublist.setTotal(total);
			sublist.setPages((int) (total /pageSize + ((total %pageSize == 0L) ? 0
						: 1)));
			sublist.setPageNum(pageNum);sublist.setPageSize(pageSize);
//			int total2=pageNum*pageSize;
//			if(sublist.getPages()==pageNum){
//				total2=total;
//			}
			
		}
		model.addAttribute("page", new PageInfo<LogBean>(sublist));
		return "other/luence/luence-list";
	}
	
	
}
