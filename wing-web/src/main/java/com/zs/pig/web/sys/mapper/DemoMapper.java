//Powered By if, Since 2014 - 2020

package com.zs.pig.web.sys.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.common.sys.model.Demo;


/**
 * 
 * @author zsCat 2016-10-20 18:00:56
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的官网
 */
public interface DemoMapper extends Mapper<Demo>{

}
