//Powered By ZSCAT, Since 2014 - 2020

package com.zs.pig.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.github.abel533.entity.Example;
import com.github.abel533.entity.Example.Criteria;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.cms.api.model.CmsArticle;
import com.zs.pig.cms.api.service.CmsArticleService;
import com.zs.pig.cms.mapper.CmsArticleMapper;
import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.common.constant.ZsCatConstant;
import com.zs.pig.common.redis.test.RedisUtils;

/**
 * 
 * @author
 */

@Service("CmsArticleService")
public class CmsArticleServiceImpl extends ServiceMybatis<CmsArticle> implements CmsArticleService {
	static Logger logger = LogManager.getLogger(CmsArticleServiceImpl.class.getName());
	@Resource
	private CmsArticleMapper CmsArticleMapper;
	@Resource
	private BlogService BlogService;
	
	/**
	 * 保存或更新
	 * 
	 * @param CmsArticle
	 * @return
	 */
	public int savecmsArticle(CmsArticle CmsArticle) {
		CmsArticle.setSiteid(Long.parseLong(RedisUtils.get(ZsCatConstant.SITEID,"1")));
		return this.save(CmsArticle);
	}

	/**
	 * 删除
	* @param CmsArticle
	* @return
	 */
	public int deleteCmsArticle(CmsArticle CmsArticle) {
		logger.log(Level.forName("DIAG", 350), "调用cms模块 - delete方法- "+CmsArticle.getId());
		return this.delete(CmsArticle);
	}

	@Override
	public PageInfo<CmsArticle> FindInfoPage(int pageNum, int pageSize, CmsArticle a,
			String orderBy) {
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("pageNum", pageNum);
		params.put("pageSize", pageSize);
		params.put("orderBy", orderBy);
		PageHelper.startPage(params);
		List<CmsArticle> list = CmsArticleMapper.findPageInfo(params);
		return new PageInfo<CmsArticle>(list);
	}

	public PageInfo<CmsArticle> selectPage(int pageNum, int pageSize, CmsArticle record,String orderSqlStr) {
		for(int i=0;i<5;i++){
			BlogService.selectPage1(1, 10, new Blog());
		}
		
		Example example = new Example(record.getClass(),false);
		Criteria criteria = example.createCriteria();
	//	criteria.andEqualTo("delFlag", Constant.DEL_FLAG_NORMAL);
		for(Map.Entry<String, Object> entry : record.entrySet()){
			if("".equals(entry.getValue())) continue;
			criteria.andEqualTo(entry.getKey(), entry.getValue());
		}
		example.setOrderByClause(orderSqlStr);
		PageHelper.startPage(pageNum, pageSize);
		List<CmsArticle> list = mapper.selectByExample(example);
		return new PageInfo<CmsArticle>(list);
	}
}
